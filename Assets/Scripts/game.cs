﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class game : MonoBehaviour {

	public float velocity = 2.8f;
	public GameObject catisol, catisag;
	private float sol, sag, solblock, sagblock, xyeni, zyeni, spawnyposition;
	private int i = 0;
	bool pressedanykey = false;
	public GameObject block;
	public GameObject[] blockclone;
	public int clonenumber = 200;
	private float piksel = 1.0f;
	private Ray ray;
	private RaycastHit raycasthit;
	public GameObject scorelabel, bestscore, gameoverlabel, resetbutton, bestscoreback, backblockbutton, backblocktext;
	public Sprite signin, signout;
	public GameObject signbutton;
	BinaryFormatter bf;
	FileStream file;
	PlayerData data;
	public GameObject coin, goldgain;
	int gold;
	bool gameover = false;
	int backnumber;
	int b = 1;
	public Color[] renk1, renk2, renk3, renk4, renk5, renk6, renk7, renk8, renk9, 
		renk10, renk11, renk12, renk13, renk14, renk15;
	Color[] renk;
	int c, d;
	int e = 1;
	bool renkbitti = false;

	// Use this for initialization
	void Start () {

		PlayGamesPlatform.Activate();

		Social.localUser.Authenticate((bool success) => {

			if(success)
			{
				signbutton.GetComponent<Image>().sprite = signout;
			}else {
				signbutton.GetComponent<Image>().sprite = signin;
			}
		});

		spawnyposition = GameObject.Find("block" + i).transform.position.y;

		GameObject.Find("block" + i).GetComponent<Rigidbody>().velocity = new Vector2(velocity, 0);

		blockclone = new GameObject[clonenumber];
		blockclone[0] = block;

		scorelabel.GetComponent<Text>().text = i.ToString();

		if(!File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
		{
			bf = new BinaryFormatter();
			file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

			data = new PlayerData();
			data.bestscore = 0;
			data.coin = 3000;

			bf.Serialize(file, data);
			file.Close();
		}

		bf = new BinaryFormatter();
		file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

		data = (PlayerData)bf.Deserialize(file);
		file.Close();

		gold = data.coin;

		coin.GetComponent<Text>().text = gold.ToString();

		backblocktext.GetComponent<Text>().text = (100 + (10 * i)).ToString();

		c = UnityEngine.Random.Range(1, 16);

		switch (c)
		{
		case 1:	renk = renk1;
			break;
		case 2:	renk = renk2;
			break;
		case 3:	renk = renk3;
			break;
		case 4:	renk = renk4;
			break;
		case 5:	renk = renk5;
			break;
		case 6:	renk = renk6;
			break;
		case 7:	renk = renk7;
			break;
		case 8:	renk = renk8;
			break;
		case 9:	renk = renk9;
			break;
		case 10: renk = renk2;
			break;
		case 11: renk = renk1;
			break;
		case 12: renk = renk12;
			break;
		case 13: renk = renk13;
			break;
		case 14: renk = renk14;
			break;
		case 15: renk = renk15;
			break;
		}

		GameObject.Find("block" + (i-1)).GetComponent<Renderer>().material.SetColor("_Color", renk[0]);
		GameObject.Find("block" + (i-1)).GetComponent<Renderer>().material.SetColor("_EmissionColor", renk[0]);
		GameObject.Find("block" + i).GetComponent<Renderer>().material.SetColor("_Color", renk[1]);
		GameObject.Find("block" + i).GetComponent<Renderer>().material.SetColor("_EmissionColor", renk[1]);

		d = 2;
	}

	public float dampTime = 0.3f;
	private Vector3 velocity3 = Vector3.zero;

	void Update () {

		Vector3 targetpos = new Vector3(13.27f, 4.5f + (0.5f * i), -4.27f);
		transform.position = Vector3.SmoothDamp(transform.position, targetpos, ref velocity3, dampTime);
	}

	void FixedUpdate () {

		if(i >= 2 && !gameover){
			backblockbutton.SetActive(true);
		} else {
			backblockbutton.SetActive(false);
		}

		if(Input.GetMouseButton(0) && !pressedanykey)
		{
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if(Physics.Raycast(ray, out raycasthit))
			{
				if(raycasthit.transform.name == "blockclick")
				{
					pressedanykey = true;

					GameObject.Find("block" + i).GetComponent<Rigidbody>().velocity = new Vector2(0, 0);

					if(b > 0)
					{
						sol = GameObject.Find("block" + (i-1)).transform.position.x - (GameObject.Find("block" + (i-1)).transform.localScale.x / 2 * piksel);
						sag = GameObject.Find("block" + (i-1)).transform.position.x + (GameObject.Find("block" + (i-1)).transform.localScale.x / 2 * piksel);

						solblock = GameObject.Find("block" + i).transform.position.x - (GameObject.Find("block" + i).transform.localScale.x / 2 * piksel);
						sagblock = GameObject.Find("block" + i).transform.position.x + (GameObject.Find("block" + i).transform.localScale.x / 2 * piksel);

						if((sagblock > sag) && ((sagblock - sag) > 0.15f))
						{
							xyeni = (sagblock - sag);
							GameObject.Find("block" + i).transform.localScale = new Vector3(GameObject.Find("block" + i).transform.localScale.x - xyeni, 
								GameObject.Find("block" + i).transform.localScale.y, GameObject.Find("block" + i).transform.localScale.z);
							GameObject.Find("block" + i).transform.position = new Vector3(GameObject.Find("block" + i).transform.position.x - (xyeni / 2), 
								GameObject.Find("block" + i).transform.position.y, GameObject.Find("block" + i).transform.position.z);
						} else if((sol > solblock) && ((sol - solblock) > 0.15f))
						{
							xyeni = (sol - solblock);
							GameObject.Find("block" + i).transform.localScale = new Vector3(GameObject.Find("block" + i).transform.localScale.x - xyeni, 
								GameObject.Find("block" + i).transform.localScale.y, GameObject.Find("block" + i).transform.localScale.z);
							GameObject.Find("block" + i).transform.position = new Vector3(GameObject.Find("block" + i).transform.position.x + (xyeni / 2), 
								GameObject.Find("block" + i).transform.position.y, GameObject.Find("block" + i).transform.position.z);
						} else {
							if(i == 0)
							{
								GameObject.Find("block" + i).transform.position = new Vector3(0, 
									GameObject.Find("block" + i).transform.position.y, GameObject.Find("block" + i).transform.position.z);
							} else {
								GameObject.Find("block" + i).transform.position = new Vector3(GameObject.Find("block" + (i-1)).transform.position.x, 
									GameObject.Find("block" + i).transform.position.y, GameObject.Find("block" + i).transform.position.z);
							}
						}

						if(GameObject.Find("block" + i).transform.localScale.x <= 0)
						{
							GameObject.Find("block" + i).transform.localScale = new Vector3(0, 0, 0);
						}
					} else 
					{
						sol = GameObject.Find("block" + (i-1)).transform.position.z - (GameObject.Find("block" + (i-1)).transform.localScale.z / 2 * piksel);
						sag = GameObject.Find("block" + (i-1)).transform.position.z + (GameObject.Find("block" + (i-1)).transform.localScale.z / 2 * piksel);

						solblock = GameObject.Find("block" + i).transform.position.z - (GameObject.Find("block" + i).transform.localScale.z / 2 * piksel);
						sagblock = GameObject.Find("block" + i).transform.position.z + (GameObject.Find("block" + i).transform.localScale.z / 2 * piksel);

						if((sagblock > sag) && ((sagblock - sag) > 0.15f))
						{
							zyeni = (sagblock - sag);
							GameObject.Find("block" + i).transform.localScale = new Vector3(GameObject.Find("block" + i).transform.localScale.x, 
								GameObject.Find("block" + i).transform.localScale.y, GameObject.Find("block" + i).transform.localScale.z - zyeni);
							GameObject.Find("block" + i).transform.position = new Vector3(GameObject.Find("block" + i).transform.position.x, 
								GameObject.Find("block" + i).transform.position.y, GameObject.Find("block" + i).transform.position.z - (zyeni / 2));
						} else if((sol > solblock) && ((sol - solblock) > 0.15f))
						{
							zyeni = (sol - solblock);
							GameObject.Find("block" + i).transform.localScale = new Vector3(GameObject.Find("block" + i).transform.localScale.x, 
								GameObject.Find("block" + i).transform.localScale.y, GameObject.Find("block" + i).transform.localScale.z - zyeni);
							GameObject.Find("block" + i).transform.position = new Vector3(GameObject.Find("block" + i).transform.position.x, 
								GameObject.Find("block" + i).transform.position.y, GameObject.Find("block" + i).transform.position.z + (zyeni / 2));
						} else {
							GameObject.Find("block" + i).transform.position = new Vector3(GameObject.Find("block" + i).transform.position.x, 
								GameObject.Find("block" + i).transform.position.y, GameObject.Find("block" + (i-1)).transform.position.z);
						}

						if(GameObject.Find("block" + i).transform.localScale.z <= 0)
						{
							GameObject.Find("block" + i).transform.localScale = new Vector3(0, 0, 0);
						}
					}

					StartCoroutine(PressedKeyChange());
				}
			}
		}

		/*
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
		*/
	}

	IEnumerator PressedKeyChange() {

		if(GameObject.Find("block" + i).transform.localScale.x <= 0 || GameObject.Find("block" + i).transform.localScale.z <= 0)
		{
			gameover = true;

			bf = new BinaryFormatter();
			file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

			data = (PlayerData)bf.Deserialize(file);
			file.Close();

            int databestscore = data.bestscore;

			if(i > data.bestscore)
			{
				bf = new BinaryFormatter();
				file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

				data = new PlayerData();
				data.bestscore = i;
                data.coin = gold;
                bf.Serialize(file, data);
                file.Close();
			} else 
			{
				bf = new BinaryFormatter();
				file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

				data = new PlayerData();
				data.bestscore = databestscore;
				data.coin = gold;
				bf.Serialize(file, data);
				file.Close();
			}

            if (Social.localUser.authenticated)
			{
				long leaderboardscore = Convert.ToInt64(i);
				Social.ReportScore(leaderboardscore, "CgkIrP211sYPEAIQAg", (bool success) => {

					if(success){
						print("oldu");
					}else {
						print("olmadı");
					}
				});
			}

			bestscore.GetComponent<Text>().text = "Best: " + data.bestscore;
			bestscoreback.SetActive(true);
			gameoverlabel.SetActive(true);
			resetbutton.SetActive(true);
		} else 
		{
			i += 1;
			b *= -1;

			if((d == 0 || d == 7) && renkbitti) {
				renkbitti = false;
				e *= -1;

				switch (c)
				{
				case 1:	renk = renk2; c = 2;
					break;
				case 2:	renk = renk3; c = 3;
					break;
				case 3:	renk = renk4; c = 4;
					break;
				case 4:	renk = renk5; c = 5;
					break;
				case 5:	renk = renk6; c = 6;
					break;
				case 6:	renk = renk15; c = 15;
					break;
				case 7:	renk = renk8; c = 8;
					break;
				case 8:	renk = renk9; c = 9;
					break;
				case 9:	renk = renk10; c = 10;
					break;
				case 10: renk = renk14; c = 14;
					break;
				case 11: renk = renk1; c = 1;
					break;
				case 12: renk = renk11; c = 11;
					break;
				case 13: renk = renk12; c = 12;
					break;
				case 14: renk = renk13; c = 13;
					break;
				case 15: renk = renk7; c = 7;
					break;
				}
			} else {
				d += e;

				if(d == 7 || d == 0) {
					renkbitti = true;
				}
			}

			goldgain.GetComponent<Animator>().SetBool("goldgain", true);
			StartCoroutine(SetAnimFalse());

			gold += 5;

			bf = new BinaryFormatter();
			file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
			data = (PlayerData)bf.Deserialize(file);
			file.Close();

			int databestscore = data.bestscore;

			bf = new BinaryFormatter();
			file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

			data = new PlayerData();
			data.bestscore = databestscore;
			data.coin = gold;
			bf.Serialize(file, data);
			file.Close();

			coin.GetComponent<Text>().text = gold.ToString();
			scorelabel.GetComponent<Text>().text = i.ToString();

			spawnyposition += 0.5f;

			if(b > 0)
			{
				blockclone[i] = (GameObject)Instantiate(GameObject.Find("block" + (i-1)), new Vector3(-2.8f + (GameObject.Find("block" + (i-1)).transform.localScale.x / 2 * piksel), 
					spawnyposition, GameObject.Find("block" + (i-1)).transform.position.z), Quaternion.identity);
				blockclone[i].name = "block" + i;
				blockclone[i].GetComponent<Rigidbody>().velocity = new Vector3(velocity + (0.07f * i), 0, 0);
			} else {
				blockclone[i] = (GameObject)Instantiate(GameObject.Find("block" + (i-1)), new Vector3(GameObject.Find("block" + (i-1)).transform.position.x, spawnyposition, 
					11.75f - (GameObject.Find("block" + (i-1)).transform.localScale.z / 2 * piksel)), Quaternion.identity);
				blockclone[i].name = "block" + i;
				blockclone[i].GetComponent<Rigidbody>().velocity = new Vector3(0, 0, -velocity - (0.07f * i));
			}

			blockclone[i].GetComponent<Renderer>().material.SetColor("_Color", renk[d]);
			blockclone[i].GetComponent<Renderer>().material.SetColor("_EmissionColor", renk[d]);

			yield return new WaitForSeconds(0.2f);
			pressedanykey = false;
		}
	}

	IEnumerator SetAnimFalse() {
		yield return new WaitForSeconds(0.1f);
		goldgain.GetComponent<Animator>().SetBool("goldgain", false);
	}

	public void BackButtonPressed()
	{
		if(b > 0)
		{
			GameObject.Find("block" + i).transform.position = new Vector3(GameObject.Find("block" + i).transform.position.x, 
				GameObject.Find("block" + i).transform.position.y - 0.5f, GameObject.Find("block" + (i-2)).transform.position.z);
		}else {
			GameObject.Find("block" + i).transform.position = new Vector3(GameObject.Find("block" + (i-2)).transform.position.x, 
				GameObject.Find("block" + i).transform.position.y - 0.5f, GameObject.Find("block" + i).transform.position.z);
		}

		GameObject.Find("block" + i).transform.localScale = GameObject.Find("block" + (i-2)).transform.localScale;

		i -= 1;
		spawnyposition -= 0.5f;
		scorelabel.GetComponent<Text>().text = i.ToString();
		Destroy(GameObject.Find("block" + i));
		GameObject.Find("block" + (i+1)).name = "block" + (i);

		gold -= (100 + (10 * backnumber));
		coin.GetComponent<Text>().text = gold.ToString();

		backnumber += 1;
		backblocktext.GetComponent<Text>().text = (100 + (10 * backnumber)).ToString();
	}
}

[Serializable]
class PlayerData
{
	public int bestscore;
	public int coin;
}
