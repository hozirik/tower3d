﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class signbutton : MonoBehaviour, IPointerClickHandler
{
	public Sprite signin, signout;

	public void OnPointerClick(PointerEventData data)
	{
		if(Social.localUser.authenticated)
		{
			PlayGamesPlatform.Instance.SignOut();
			gameObject.GetComponent<Image>().sprite = signin;
		} else
		{
			Social.localUser.Authenticate((bool success) => {

				if(success)
				{
					gameObject.GetComponent<Image>().sprite = signout;
				}else {
					gameObject.GetComponent<Image>().sprite = signin;
				}
			});
		}
	}

	private void Update()
	{

	}
}
